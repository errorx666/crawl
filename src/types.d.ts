declare interface BoxSides<T> {
	readonly forward: T;
	readonly backward: T;
	readonly up: T;
	readonly down: T;
	readonly left: T;
	readonly right: T;
}

declare interface Box<T> {
	readonly inside: BoxSides<T>;
	readonly outside: BoxSides<T>;
}

declare interface BoxSideDesc {
	readonly solid: boolean;
}

declare interface BoxDesc extends Box<BoxSideDesc> {}
