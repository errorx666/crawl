import { PerspectiveCamera, WebGLRenderer, Scene, Color, AmbientLight, PointLight, DirectionalLight, Geometry, Mesh, Material, Group } from 'three';
import { EffectComposer } from 'three/examples/jsm/postprocessing/EffectComposer';
import { RenderPass } from 'three/examples/jsm/postprocessing/RenderPass';
import { ShaderPass } from 'three/examples/jsm/postprocessing/ShaderPass';
import { FXAAShader } from 'three/examples/jsm/shaders/FXAAShader';
import { geometry } from '~geometry';
import { materials } from '~materials';

const scene = new Scene;
const renderer = new WebGLRenderer( {
	antialias: true,
} );
renderer.autoClear = false;
renderer.setSize( 1920, 1080, false );
document.body.appendChild( renderer.domElement );

const canvas = renderer.domElement;
const { width, height } = canvas;
const aspect = width / height;
const camera = new PerspectiveCamera( 50, aspect, 0.1, 1000 );
camera.position.set( 0, 0, 0 );
scene.add( camera );

{
	const light = new AmbientLight( ( new Color ).setRGB( 1, 1, 1 ), .2 );
	scene.add( light );
}

{
	const light = new DirectionalLight( ( new Color ).setRGB( 1, 1, 1 ), .5 );
	scene.add( light );
}

{
	const light = new PointLight( ( new Color ).setRGB( 1, 1, 1 ), 1, 50, 1 );
	light.position.set( 0, 3, 0 );
	scene.add( light );
}

const boxMesh = new Mesh<Geometry, Material[]>(
	geometry.cubeFace.all,
	Array.from( { length: 12 } ).map( () => materials.empty )
);

const group = new Group;
group.position.set( 0, 0, -10 );
scene.add( group );
const mat: Material[] = [
	...Array.from( { length: 20 } ).map( () => materials.empty ),
	...Object.values( materials.solid )
];
for( let x = -10; x < 10; ++x ) {
	for( let y = -2; y < 2; ++y ) {
		for( let z = -10; z < 10; ++z ) {
			const mesh = boxMesh.clone();
			mesh.material = Array.from( { length: 12 } ).map( () => mat[ Math.floor( Math.random() * mat.length ) ] );
			mesh.translateX( x );
			mesh.translateY( y );
			mesh.translateZ( z );
			group.add( mesh );
		}
	}
}

const renderPass = new RenderPass( scene, camera );
const fxaaPass = new ShaderPass( FXAAShader );
const effectComposer = new EffectComposer( renderer );
effectComposer.addPass( renderPass );
effectComposer.addPass( fxaaPass );

const pixelRatio = renderer.getPixelRatio();
fxaaPass.material.uniforms[ 'resolution' ].value.x = 1 / ( width * pixelRatio );
fxaaPass.material.uniforms[ 'resolution' ].value.y = 1 / ( height * pixelRatio );

renderer.setAnimationLoop( ( time: number ) => {
	effectComposer.render();
} );
