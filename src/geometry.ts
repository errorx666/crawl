import { PlaneGeometry, Matrix4 } from 'three';

export namespace geometry {
	export namespace cubeFace {
		export namespace inside {
			export const forward = new PlaneGeometry( 1, 1, 1, 1 ).translate( 0, 0, -.5 );
			export const backward = new PlaneGeometry( 1, 1, 1, 1 ).rotateX( Math.PI ).translate( 0, 0, .5 );
			export const up = new PlaneGeometry( 1, 1, 1, 1 ).rotateX( Math.PI * .5 ).translate( 0, .5, 0 );
			export const down = new PlaneGeometry( 1, 1, 1, 1 ).rotateX( Math.PI * 1.5 ).translate( 0, -.5, 0 );
			export const left = new PlaneGeometry( 1, 1, 1, 1 ).rotateY( Math.PI * .5 ).translate( -.5, 0, 0 );
			export const right = new PlaneGeometry( 1, 1, 1, 1 ).rotateY( Math.PI * 1.5 ).translate( .5, 0, 0 );
			export const all = forward.clone();
			all.merge( backward, ( new Matrix4 ).identity(), 1 );
			all.merge( up, ( new Matrix4 ).identity(), 2 );
			all.merge( down, ( new Matrix4 ).identity(), 3 );
			all.merge( left, ( new Matrix4 ).identity(), 4 );
			all.merge( right, ( new Matrix4 ).identity(), 5 );
		}
		export namespace outside {
			export const forward = new PlaneGeometry( 1, 1, 1, 1 ).rotateX( Math.PI ).translate( 0, 0, -.5 );
			export const backward = new PlaneGeometry( 1, 1, 1, 1 ).translate( 0, 0, .5 );
			export const up = new PlaneGeometry( 1, 1, 1, 1 ).rotateX( Math.PI * 1.5 ).translate( 0, .5, 0 );
			export const down = new PlaneGeometry( 1, 1, 1, 1 ).rotateX( Math.PI * .5 ).translate( 0, -.5, 0 );
			export const left = new PlaneGeometry( 1, 1, 1, 1 ).rotateY( Math.PI * 1.5 ).translate( -.5, 0, 0 );
			export const right = new PlaneGeometry( 1, 1, 1, 1 ).rotateY( Math.PI * .5 ).translate( .5, 0, 0 );
			export const all = forward.clone();
			all.merge( backward, ( new Matrix4 ).identity(), 1 );
			all.merge( up, ( new Matrix4 ).identity(), 2 );
			all.merge( down, ( new Matrix4 ).identity(), 3 );
			all.merge( left, ( new Matrix4 ).identity(), 4 );
			all.merge( right, ( new Matrix4 ).identity(), 5 );
		}
		export const all = inside.all.clone();
		all.merge( outside.all, ( new Matrix4 ).identity(), 6 );
	}
}
