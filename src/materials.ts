import { MeshStandardMaterial, Color, MeshBasicMaterial } from 'three';

export namespace materials {
	export const empty = new MeshBasicMaterial( { visible: false } );
	export namespace solid {
		export const red = new MeshStandardMaterial( {
			color: ( new Color ).setRGB( 1, 0, 0 )
		} );
		export const green = new MeshStandardMaterial( {
			color: ( new Color ).setRGB( 0, 1, 0 )
		} );
		export const blue = new MeshStandardMaterial( {
			color: ( new Color ).setRGB( 0, 0, 1 )
		} );
	}
	export namespace wireframe {
		export const red = new MeshStandardMaterial( {
			color: ( new Color ).setRGB( 1, 0, 0 ),
			wireframe: true
		} );
		export const green = new MeshStandardMaterial( {
			color: ( new Color ).setRGB( 0, 1, 0 ),
			wireframe: true
		} );
		export const blue = new MeshStandardMaterial( {
			color: ( new Color ).setRGB( 0, 0, 1 ),
			wireframe: true
		} );
	}
}
