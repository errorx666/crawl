import path from 'path';
import ForkTsCheckerWebpackPlugin from 'fork-ts-checker-webpack-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import TsconfigPathsWebpackPlugin from 'tsconfig-paths-webpack-plugin';

const mode = 'development';

const loaders = {
	babel: { loader: 'babel-loader', options: {
		babelrc: false,
		extends: path.resolve( __dirname, 'src', '.babelrc' )
	} },
	css: { loader: 'css-loader', options: {} },
	typescript: { loader: 'ts-loader', options: {
		transpileOnly: true
	} },
	yaml: { loader: 'yaml-loader', options: { safe: false } }
};

/** @type {import('webpack').Configuration} */
const config = {
	devtool: 'source-map',
	entry: {
		index: [
			path.resolve( __dirname, 'src', 'index.css' ),
			path.resolve( __dirname, 'src', 'index.ts' )
		]
	},
	experiments: {
		topLevelAwait: true
	},
	mode,
	module: {
		rules: [
			{ test: /\.css/i, use: [ MiniCssExtractPlugin.loader, loaders.css ] },
			{ test: /\.ya?ml/i, use: [ loaders.yaml ] },
			{ test: /\.ts/i, include: path.resolve( __dirname, 'src' ), use: [ loaders.babel, loaders.typescript ] }
		]
	},
	output: {
		path: path.resolve( __dirname, 'dist' )
	},
	plugins: [
		new HtmlWebpackPlugin( {
			template: path.resolve( __dirname, 'src', 'index.ejs' )
		} ),
		new ForkTsCheckerWebpackPlugin( {
			eslint: {
				files: [
					'src/**/*.ts'
				]
			}
		} ),
		new MiniCssExtractPlugin
	],
	resolve: {
		extensions: [ '.ts', '.js', '.json' ],
		plugins: [
			new TsconfigPathsWebpackPlugin
		]
	}
};

export default config;
