module.exports = {
	ui: false,
	files: [
		'./dist/**/*'
	],
	watch: true,
	watchOptions: {
		ignoreInitial: true
	},
	server: {
		baseDir: './dist',
		middleware: {
			0: null
		}
	},
	port: 8080,
	ghostMode: false,
	rewriteRules: [],
	open: true,
	notify: true,
	reloadDebounce: 500,
	reloadThrottle: 0,
	localOnly: false
};
